package com.jesusceladev.notes.usecases

import com.jesusceladev.notes.data.NoteRepository
import com.jesusceladev.notes.domain.Note
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class GetNotes(private val noteRepository: NoteRepository) {

    suspend fun run(): List<Note> {
        return withContext(Dispatchers.IO) { noteRepository.getNotes() }
    }

    suspend fun runObserve(): Flow<List<Note>> {
        return withContext(Dispatchers.IO) {
            noteRepository.observeNotes()
        }
    }
}