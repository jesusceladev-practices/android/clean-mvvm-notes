package com.jesusceladev.notes.usecases

import com.jesusceladev.notes.data.NoteRepository
import com.jesusceladev.notes.domain.Note
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SaveOrUpdateNote(private val noteRepository: NoteRepository) {

    suspend fun run(note: Note): Int {
        return withContext(Dispatchers.IO) {
            note.uid?.let { noteRepository.updateNote(note) } ?: noteRepository.saveNote(note)
        }
    }
}