package com.jesusceladev.notes.domain

import java.util.*

data class Note(
    val uid: Int?,
    val name: String,
    val description: String?,
    val type: NoteType,
    val createdDate: Date?,
    val modifiedDate: Date?
) {
}