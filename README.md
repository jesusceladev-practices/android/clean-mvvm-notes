# CLEAN-MVVM-NOTES

Practice of MVVM presenter pattern with Clean Architecture.

**Layers used (represented as kotlin modules):**
- **Domain**: collection of entity objects and related business logic.
- **Data**: an abstract definition of the different data sources.
- **UseCases**: actions that the user can trigger.
- **App**: it is the layer that interacts with the user interface (MVVM pattern)


**Notes:**
- Koin as a service locator to inject dependencies
- DataBinding in NoteDetailActivity
- Room to store local data


**Future lines:**
- Viewmodel injected to activity by Koin
- Implements ViewBinding/DataBinding in adapter
- Use fragment


**Bugs:**
- Weird fab behavior with snackbar
- Observe message when changing screen orientation



