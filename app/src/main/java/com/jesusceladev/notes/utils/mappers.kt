package com.jesusceladev.notes.utils

import android.text.format.DateFormat
import com.jesusceladev.notes.datasource.entity.NoteVO
import com.jesusceladev.notes.domain.Note
import com.jesusceladev.notes.domain.NoteType
import com.jesusceladev.notes.ui.common.NoteView
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

private val DATE_PATTERN: String = "dd/MM/yyyy HH:mm:ss"
var formatter: SimpleDateFormat = SimpleDateFormat(DATE_PATTERN)

fun NoteVO.toNote(): Note =
    Note(
        id,
        name,
        description,
        NoteType.valueOf(type),
        createdDate,
        modifiedDate
    )

fun Note.toNoteVO(): NoteVO =
    NoteVO(uid, name, description, type.name, createdDate ?: Date(), modifiedDate ?: Date())

fun Note.toNoteView(): NoteView =
    NoteView(
        uid,
        name,
        description,
        type.name,
        createdDate?.toStringDateFormat(),
        createdDate?.toStringTimeFormat(),
        modifiedDate?.toStringDateFormat(),
        modifiedDate?.toStringTimeFormat()
    )

fun NoteView.toNote(): Note =
    Note(
        uid,
        name,
        description,
        NoteType.valueOf(type),
        (createdDate + " " + createdTime).toDate() ?: Date(),
        Date()
    )

fun Date.toStringDateFormat(): String {
    return DateFormat.format(DATE_PATTERN, this).toString().split(" ")[0]
}

fun Date.toStringTimeFormat(): String {
    return DateFormat.format(DATE_PATTERN, this).toString().split(" ")[1]
}

fun String.toDate(): Date? {
    return try {
        formatter.parse(this)
    } catch (ex: ParseException) {
        null
    }
}

