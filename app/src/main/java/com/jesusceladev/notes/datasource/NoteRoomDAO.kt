package com.jesusceladev.notes.datasource

import androidx.room.*
import com.jesusceladev.notes.datasource.entity.NoteVO
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteRoomDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveNoteRoom(note: NoteVO): Long

    @Update
    fun updateNoteRoom(note: NoteVO): Int

    @Query("SELECT * FROM notes")
    fun getNotesRoom(): List<NoteVO>

    @Query("SELECT * FROM notes")
    fun observeNotesRoom(): Flow<List<NoteVO>>

    @Query("SELECT * FROM notes WHERE id = :id")
    fun getNoteRoom(id: Int): NoteVO

    @Delete
    fun deleteNoteRoom(note: NoteVO): Int

    @Query("SELECT count(*) FROM notes WHERE id = :id")
    fun existNoteRoom(id: Int): Int
}