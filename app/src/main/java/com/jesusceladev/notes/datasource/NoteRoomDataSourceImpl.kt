package com.jesusceladev.notes.datasource

import com.jesusceladev.notes.utils.toNote
import com.jesusceladev.notes.utils.toNoteVO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class NoteRoomDataSourceImpl(private val notesDao: NoteRoomDAO) :
    com.jesusceladev.notes.data.NoteRepository.NotePersistenceDataSource {

    override fun saveNote(note: com.jesusceladev.notes.domain.Note): Int {
        return notesDao.saveNoteRoom(note.toNoteVO()).toInt()
    }

    override fun updateNote(note: com.jesusceladev.notes.domain.Note): Int {
        return notesDao.updateNoteRoom(note.toNoteVO())
    }

    override fun existNote(uid: Int): Boolean {
        return (notesDao.existNoteRoom(uid) > 0)
    }

    override fun getNotes(): List<com.jesusceladev.notes.domain.Note> {
        return notesDao.getNotesRoom().map { it.toNote() }
    }

    override fun observeNotes(): Flow<List<com.jesusceladev.notes.domain.Note>> {
        return notesDao.observeNotesRoom().map { it.map { it.toNote() } }
    }

    override fun getNote(uid: Int): com.jesusceladev.notes.domain.Note {
        return notesDao.getNoteRoom(uid).toNote()
    }

    override fun deleteNote(uid: Int): Int {
        if (notesDao.existNoteRoom(uid) > 0) {
            val note = notesDao.getNoteRoom(uid)
            return notesDao.deleteNoteRoom(note)
        }
        return -1
    }
}