package com.jesusceladev.notes.ui.common

data class NoteChangeEvent(val note: NoteView?, val change: NoteChangeType)