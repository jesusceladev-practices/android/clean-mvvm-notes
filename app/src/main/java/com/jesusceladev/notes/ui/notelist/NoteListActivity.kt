package com.jesusceladev.notes.ui.notelist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.jesusceladev.notes.R
import com.jesusceladev.notes.ui.adapters.NoteItemAdapter
import com.jesusceladev.notes.ui.common.NoteChangeEvent
import com.jesusceladev.notes.ui.common.NoteChangeType
import com.jesusceladev.notes.ui.common.NoteView
import com.jesusceladev.notes.ui.notedetail.NoteDetailActivity
import com.jesusceladev.notes.utils.*
import com.jesusceladev.notes.databinding.ActivityNoteListBinding

class NoteListActivity : AppCompatActivity() {

    private lateinit var noteListViewModel: NoteListViewModel
    private lateinit var noteItemAdapter: NoteItemAdapter
    private lateinit var binding: ActivityNoteListBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNoteListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        noteListViewModel = ViewModelProvider(this)[NoteListViewModel::class.java]

        // init empty list
        binding.notesRecycler.layoutManager =
            GridLayoutManager(this, if (resources.configuration.isOrientationVertical()) 2 else 3)
        noteItemAdapter = NoteItemAdapter(listOf(), { navigateToDetail(it) }, { confirmDelete(it) })
        binding.notesRecycler.adapter = noteItemAdapter

        // observers
        noteListViewModel.noteList.observe(this, androidx.lifecycle.Observer { notes ->
            if (notes.isEmpty()) hideNoteListAndShowText()
            else refreshNoteList(notes)
        })

        noteListViewModel.noteChange.observe(this, {
            when (it.change) {
                NoteChangeType.DELETE -> binding.noteListLayout.snackBar(getString(R.string.action_note_deleted))
            }
        })

        // listeners
        binding.noteAddBtn.setOnClickListener {
            startActivity(Intent(this, NoteDetailActivity::class.java))
        }

        noteListViewModel.observeNotes()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let { setIntent(intent) }
    }

    override fun onResume() {
        super.onResume()
        checkActions()
    }

    private fun checkActions() {
        if (intent.getBooleanExtra(NOTE_DETAIL_ACTION_DELETE_EXTRA, false)) {
            noteListViewModel.setChangeEvent(NoteChangeEvent(null, NoteChangeType.DELETE))
        }
    }

    private fun navigateToDetail(note: NoteView) {
        startActivity(Intent(this, NoteDetailActivity::class.java).apply {
            putExtra(NOTE_DETAIL_EXTRA, note)
        })
    }

    private fun confirmDelete(note: NoteView): Boolean {
        confirmDialog(
            getString(R.string.dialog_delete_title),
            getString(R.string.dialog_delete_message),
            getString(R.string.dialog_text_eliminar)
        )
        { noteListViewModel.deleteNote(note) }
        return true
    }

    private fun refreshNoteList(notes: List<NoteView>?) {
        showNoteListAndHideText()
        noteItemAdapter.updateNoteList(notes ?: listOf())
        noteItemAdapter.notifyDataSetChanged()
    }

    private fun showNoteListAndHideText() {
        binding.textNoNotes.visible(false)
        binding.notesRecycler.visible(true)
    }

    private fun hideNoteListAndShowText() {
        binding.textNoNotes.visible(true)
        binding.notesRecycler.visible(false)
    }
}