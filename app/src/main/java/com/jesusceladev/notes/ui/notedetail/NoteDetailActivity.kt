package com.jesusceladev.notes.ui.notedetail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.jesusceladev.notes.R
import com.jesusceladev.notes.ui.common.NoteChangeType
import com.jesusceladev.notes.ui.common.NoteView
import com.jesusceladev.notes.ui.notelist.NoteListActivity
import com.jesusceladev.notes.databinding.ActivityNoteDetailBinding
import com.jesusceladev.notes.utils.*

class NoteDetailActivity : AppCompatActivity() {

    private lateinit var noteDetailViewModel: NoteDetailViewModel
    private lateinit var binding: ActivityNoteDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNoteDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.noteDetailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        noteDetailViewModel = ViewModelProvider(this)[NoteDetailViewModel::class.java]
        binding.vm = noteDetailViewModel
        binding.lifecycleOwner = this

        noteDetailViewModel.setNoteView(intent.getSerializableExtra(NOTE_DETAIL_EXTRA) as NoteView?)

        noteDetailViewModel.noteChange.observe(this) {
            when (it.change) {
                NoteChangeType.CREATE -> {
                    binding.noteDetailLayout.snackBar(getString(R.string.action_note_saved))
                    checkDeleteActionVisibility()
                }
                NoteChangeType.DELETE -> navigateToListDeleteAction()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.note_detail_menu, menu)
        checkDeleteActionVisibility()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        binding.noteDetailToolbar.menu.findItem(R.id.menu_item_save).itemId -> {
            hideKeyboard()
            noteDetailViewModel.createOrUpdateNote()
            true
        }
        binding.noteDetailToolbar.menu.findItem(R.id.menu_item_delete).itemId -> {
            confirmDelete()
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun checkDeleteActionVisibility() {
        noteDetailViewModel.getNoteView().uid?.let {
            binding.noteDetailToolbar.menu.findItem(R.id.menu_item_delete).setVisible(true)
        }
    }

    private fun confirmDelete(): Boolean {
        confirmDialog(
            getString(R.string.dialog_delete_title),
            getString(R.string.dialog_delete_message),
            getString(R.string.dialog_text_eliminar)
        )
        { noteDetailViewModel.deleteNote() }
        return true
    }

    private fun navigateToListDeleteAction() {
        startActivity(Intent(this, NoteListActivity::class.java).apply {
            putExtra(NOTE_DETAIL_ACTION_DELETE_EXTRA, true)
        })
        finish()
    }
}